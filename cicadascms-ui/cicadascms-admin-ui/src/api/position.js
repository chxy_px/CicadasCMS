import request from '@/utils/request'
import QueryString from 'qs'

const urlPrefix = '/system/position'

export function page(query) {
    return request({
        url: urlPrefix + '/page',
        method: 'get',
        params: query
    })
}

export function getPositionList() {
    return request({
        url: urlPrefix + '/list',
        method: 'get'
    })
}

export function findById(id) {
    return request({
        url: urlPrefix + '/' + id,
        method: 'get'
    })
}

export function deleteById(id) {
    return request({
        url: urlPrefix + '/' + id,
        method: 'delete'
    })
}

export function save(data) {
    return request({
        url: urlPrefix,
        method: 'post',
        data
    })
}

export function updateById(data) {
    return request({
        url: urlPrefix,
        method: 'put',
        data
    })
}
