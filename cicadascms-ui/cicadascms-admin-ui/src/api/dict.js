import request from '@/utils/request'
import QueryString from 'qs'

const urlPrefix = '/system/dict'

export function page(query) {
  return request({
    url: urlPrefix + '/page',
    method: 'get',
    params: query,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      return QueryString.stringify(data)
    }]
  })
}

export function getData(code) {
  return request({
    url: urlPrefix + '/data/' + code,
    method: 'get'
  })
}

export function list(query) {
  return request({
    url: urlPrefix + '/list',
    method: 'get',
    params: query,
    transformRequest: [function (data) {
      return QueryString.stringify(data)
    }]
  })
}

// 角色类型
export function rolesType(query) {
  return request({
    url: urlPrefix + '/data/roleType',
    method: 'get',
    params: query
  })
}

export function findById(id) {
  return request({
    url: urlPrefix + '/' + id,
    method: 'get'
  })
}

export function deleteById(id) {
  return request({
    url: urlPrefix + '/' + id,
    method: 'delete'
  })
}

export function save(data) {
  return request({
    url: urlPrefix,
    method: 'post',
    data
  })
}

export function updateById(data) {
  return request({
    url: urlPrefix,
    method: 'put',
    data
  })
}
