import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: '仪表板', icon: 'dashboard', affix: true }
      }
    ],
    hidden: true
  },
  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: '个人中心',
        meta: { title: '个人中心', icon: 'user', noCache: true }
      }
    ]
  },

  {
    path: '/content',
    component: Layout,
    redirect: '/content/index',
    meta: { title: '内容管理', icon: 'table', noCache: true },
    children: [
      {
        path: 'index',
        component: () => import('@/views/cms/site/index'),
        name: '内容管理',
        meta: { title: '内容管理', icon: 'star', noCache: true }
      },
      {
        path: 'channel',
        component: () => import('@/views/cms/channel/index'),
        name: '栏目管理',
        meta: { title: '栏目管理', icon: 'list', noCache: true }
      }
    ]
  },
  {
    path: '/site',
    component: Layout,
    redirect: '/site/setting',
    meta: { title: '系统管理', icon: 'international', noCache: true },
    children: [
      {
        path: 'setting',
        component: () => import('@/views/cms/site/index'),
        name: '站点设置',
        meta: { title: '站点管理', icon: 'star', noCache: true }
      },
      {
        path: 'resource',
        component: () => import('@/views/cms/site/index'),
        name: '资源管理',
        meta: { title: '资源管理', icon: 'list', noCache: true }
      },
      {
        path: 'model',
        component: () => import('@/views/cms/model/index'),
        name: '模型管理',
        meta: { title: '模型管理', icon: 'layer-group-fill', noCache: true }
      }
    ]
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
