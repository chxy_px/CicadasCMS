/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.utils;

import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.tree.TreeNode;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * TreeUtils
 *
 * @author Jin
 */
@UtilityClass
public class TreeUtils {

    /**
     * list to tree
     *
     * @param list
     * @param <T>
     * @return
     */
    public static <T extends TreeNode> List<T> toTree(List<T> list) {
        //返回的节点树
        List<T> rootNodes = new ArrayList<>();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            T next = it.next();
            //parent（上级Id）为0的是根节点
            if (Constant.PARENT_ID.toString().equals(next.getParentNodeId().toString())) {
                rootNodes.add(next);
                it.remove();
            }
        }
        //遍历，找到二级节点
        for (T secondNode : rootNodes) {
            List<T> child = getChild(list, secondNode.getCurrentNodeId());
            secondNode.setChildren(child);
        }
        return rootNodes;

    }

    /**
     * 查子节点
     *
     * @param list
     * @param parentId
     * @param <T>
     * @return
     */
    private static <T extends TreeNode> List<T> getChild(List<T> list, Serializable parentId) {

        //子节点列表
        List<T> childList = new ArrayList<>();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            T node = it.next();
            if (parentId.toString().equals(node.getParentNodeId().toString())) {
                childList.add(node);
                it.remove();
            }
        }

        //遍历 递归获取子节点的子节点
        for (T childNode : childList) {
            List<T> child = getChild(list, childNode.getCurrentNodeId());
            childNode.setChildren(child);
        }
        //递归出口  childList长度为0
        if (childList.size() == 0) {
            return null;
        }
        return childList;
    }

}
