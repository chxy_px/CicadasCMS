/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.base;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.utils.WarpsUtils;

import java.util.List;

/**
 * BaseService
 *
 * @author Jin
 */
public abstract class BaseService<M extends BaseMapper<E>, E> extends ServiceImpl<M, E> {

    protected String cacheName() {
        throw new ServiceException("");
    }

    protected Object getCache(String key) {
        return null;
    }

    protected void putCache(String key, Object e) {

    }

    protected void removeCache(String key) {

    }

    public LambdaQueryWrapper<E> getLambdaQueryWrapper() {
        return new LambdaQueryWrapper<>();
    }

    public LambdaQueryWrapper<E> getLambdaQueryWrapper(E entity) {
        return new LambdaQueryWrapper<>(entity);
    }

    public List<E> findAll() {
        return baseMapper.selectList(Wrappers.emptyWrapper());
    }

    protected <T> T copyTo(Object object, Class<T> clazz) {
        return WarpsUtils.copyTo(object, clazz);
    }
}
