/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.constant;

/**
 * Constant
 *
 * @author Jin
 */
public class Constant {

    public static final String UTF_8 = "UTF-8";

    public static final String APPLICATION_JSON = "application/json";

    public static final String PREFIX = "cicadascms_";

    public static final String OAUTH_LICENSE = "made by CicadasCMS v2.0";

    public static final String OAUTH_PREFIX = "oauth:";

    public static final String MODE_DEV = "dev";
    public static final String MODE_PROD = "prof";

    public static final String DEFAULT_FOLDER_NAME = "cicadascms-appdata";

    //Api 返回值常量
    public static final Integer SUCCESS_CODE = 20000;

    public static final Integer ERROR_CODE = 30000;

    public static final Integer PARENT_ID = 0;
    public static final Integer PAGE_NUM = 1;
    public static final Integer PAGE_SIZE = 20;


    //验证相关常量

    public static final String VALIDATE_PARAMETER_NAME_CODE = "code";

    public static final String VALIDATE_PARAMETER_NAME_DEVICE_ID = "deviceId";

    public static final String VALIDATE_PARAMETER_NAME_MOBILE = "mobile";

    public static final String VALIDATE_REDIS_CODE_KEY = "validate_code";

    public static final int VALIDATE_CODE_SIZE = 6;

    public static final int VALIDATE_CODE_TIME = 300;

    public static final String VALIDATE_TEST_CLIENT = "test";
}
