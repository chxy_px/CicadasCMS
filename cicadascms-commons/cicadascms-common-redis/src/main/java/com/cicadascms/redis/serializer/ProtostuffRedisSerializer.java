/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.redis.serializer;

import com.cicadascms.common.func.Fn;
import com.cicadascms.redis.utils.ProtostuffSerializerUtil;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

public class  ProtostuffRedisSerializer<T> implements RedisSerializer<T> {

    private final RedisObjectSerializer redisObjectSerializer = new RedisObjectSerializer();

    @Override
    public byte[] serialize(T obj) throws SerializationException {
        ObjectWrapper objectWrapper = new ObjectWrapper(obj);
        return ProtostuffSerializerUtil.serialize(objectWrapper);
    }

    @Override
    public T deserialize(byte[] bytes) throws SerializationException {
        ObjectWrapper<T> objectWrapper = ProtostuffSerializerUtil.deserialize(bytes, ObjectWrapper.class);
        if (Fn.isNull(objectWrapper.getData())) {
            return (T) redisObjectSerializer.deserialize(bytes);
        }
        return objectWrapper.getData();
    }

    public static class ObjectWrapper<T> {
        private T data;

        public ObjectWrapper() {
        }

        public ObjectWrapper(T data) {
            this.data = data;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }
    }
}
