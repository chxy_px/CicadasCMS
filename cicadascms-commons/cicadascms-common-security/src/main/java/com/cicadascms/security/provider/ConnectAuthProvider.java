/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.security.provider;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * ConnectAuthProvider 主要用于第三方账号关联
 *
 * @author Jin
 */
public abstract class ConnectAuthProvider<T extends AbstractAuthenticationToken, E> implements AuthenticationProvider {

    protected abstract T process(Authentication authentication) throws Exception;

    protected E connect(String principal, boolean createUser) throws Exception {
        throw new IllegalArgumentException("");
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            return process(authentication);
        } catch (Exception e) {
            throw new InternalAuthenticationServiceException(e.fillInStackTrace().toString());
        }
    }
}
