CicadasCMS 2.0 开发中... 老版本请看master分支

QQ交流群:1482196

#### 项目结构

```
CicadasCMS v2.0
    ├── cicadascms-builder                              
    ├── cicadascms-commons                 
            ├── cicadascms-common-core                  
            ├── cicadascms-common-datasource            
            ├── cicadascms-common-logger               
            ├── cicadascms-common-lucene                
            ├── cicadascms-common-mybatis               
            ├── cicadascms-common-plugin                
            ├── cicadascms-common-redis                 
            ├── cicadascms-common-security            
            └── cicadascms-common-weixin                
    ├── cicadascms-data                    
            ├── cicadascms-domian                      
            └── cicadascms-mapper                      
    ├── cicadascms-launcher                           
    ├── cicadascms-modules                   
            ├── cicadascms-admin                        
                    ├── cicadascms-admin-app                
                    ├── cicadascms-admin-logic                
                    └── cicadascms-admin-static                 
            ├── cicadascms-front                       
                    ├── cicadascms-front-app                 
                    ├── cicadascms-front-logic                 
                    └── cicadascms-front-static                 
            └── cicadascms-system                      
                    ├── cicadascms-system-app                 
                    ├── cicadascms-system-logic                 
                    └── cicadascms-system-static  
    ├── cicadascms-support                               
    ├── cicadascms-ui                                      
    └── doc                       
``` 


#### 界面预览

![后台登录界面](docs/picture/login.png "后台登录界面")

<img src="https://img.shields.io/badge/JDK-1.8+-green.svg" alt="jdk version">
<img src="https://img.shields.io/badge/Spring%20Boot-2.4.2-blue.svg" alt="Coverage Status">
<img src="https://img.shields.io/badge/Mybatis%20Plus-3.4.2-red.svg" alt="Coverage Status">




