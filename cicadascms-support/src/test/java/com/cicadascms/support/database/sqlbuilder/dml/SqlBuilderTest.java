package com.cicadascms.support.database.sqlbuilder.dml;

import com.cicadascms.support.database.sqlbuilder.dml.impl.MysqlInsertDataSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.dml.impl.MysqlUpdateDataSqlBuilder;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * UpdateDateSqlBuilderTest
 *
 * @author Jin
 */
public class SqlBuilderTest {

    @Test
    public  void test() {
        UpdateDateSqlBuilder insertDataSqlBuilder = new MysqlUpdateDataSqlBuilder();
        List<String> fields = new ArrayList<>();
        fields.add("attr_class_id");
        fields.add("attr_name");
        fields.add("attr_store_type");
        fields.add("attr_location");
        List<Object> paramValues = new ArrayList<>();
        paramValues.add(1);
        paramValues.add("111");
        paramValues.add(3);
        paramValues.add("400004444");
        paramValues.add(5);

        String sql = insertDataSqlBuilder.tableName("sys_attr").update("id", 1, fields, paramValues).buildSql();

        System.out.println(sql);
    }

    @Test
    public  void test2()  {
        InsertDataSqlBuilder insertDataSqlBuilder = new MysqlInsertDataSqlBuilder();
        List<String> fields = new ArrayList<>();
        fields.add("attr_class_id");
        fields.add("attr_name");
        fields.add("attr_store_type");
        fields.add("attr_location");
        List<Object> paramValues = new ArrayList<>();
        paramValues.add(1);
        paramValues.add("111");
        paramValues.add(3);
        paramValues.add("44444444444444");
        paramValues.add(5);

        String sql = insertDataSqlBuilder.tableName("sys_attr").insert(fields, paramValues).buildSql();

        sql = sql.substring(0, sql.length() - 1);
        System.out.println(sql);
    }

}
