package com.cicadascms.support.xss.util;


import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

public class XssUtilTest {

    @Test
    public void test() {
        String s = "[{KEY:\" <script>alert(1)</script> \";d:'fffff'},\" <script>alert(1)</script> \",'hahah哈哈哈哈']";
        String res = new String(XssUtil.cleanXSS(s.getBytes()), StandardCharsets.UTF_8);
        System.out.println(res);
    }

}
