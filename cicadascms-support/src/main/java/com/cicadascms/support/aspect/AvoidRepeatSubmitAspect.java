/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.aspect;

import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.ControllerUtil;
import com.cicadascms.redis.utils.RedisUtils;
import com.cicadascms.support.annotation.AvoidRepeatSubmit;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * AvoidRepeatSubmitAspect 基于redis的放重提交注解
 *
 * @author Jin
 */
@Slf4j
@Component
@Aspect
public class AvoidRepeatSubmitAspect {

    @Around("@annotation(avoidRepeatSubmit)")
    public Object around(ProceedingJoinPoint point, AvoidRepeatSubmit avoidRepeatSubmit) throws Throwable {
        HttpServletRequest request = ControllerUtil.getHttpServletRequest();
        if (avoidRepeatSubmit != null) {
            String accessToken = getAccessToken(request);
            String redisKey = getAvoidRepeatSubmitRedisKey(accessToken, request.getMethod(), request.getRequestURI());
            String redisValue = RedisUtils.get(redisKey);
            if (Fn.isEmpty(redisValue)) {
                RedisUtils.setEx(redisKey, request.getMethod(), avoidRepeatSubmit.lockTime() * 5, TimeUnit.MILLISECONDS);
            } else {
                if (Fn.equal(request.getMethod(), redisValue)) throw new ServiceException("请勿重复提交！");
            }
        }
        return point.proceed();
    }

    private String getAvoidRepeatSubmitRedisKey(String token, String method, String path) {
        return Constant.PREFIX + "avoid_repeat_submit_lock:" + method + ":" + token + ":" + path;
    }

    private static String getAccessToken(HttpServletRequest request) {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (header == null || !header.startsWith("Bearer ")) {
            throw new UnapprovedClientAuthenticationException("请求头中没有找到Token信息！");
        }
        return header.substring(7);
    }

}
