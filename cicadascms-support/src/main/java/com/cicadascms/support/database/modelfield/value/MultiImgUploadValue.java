/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.database.modelfield.value;

import com.cicadascms.common.func.Fn;
import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import com.cicadascms.support.database.modelfield.model.ImgUploadItem;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class MultiImgUploadValue implements ModelFieldValue<List<ImgUploadItem>> {

    private String type = ModelFieldTypeEnum.MULTI_IMG_UPLOAD_NAME;

    private List<ImgUploadItem> imgList = new ArrayList<>();

    public List<ImgUploadItem> getImgList() {
        if (Fn.isEmpty(imgList)) {
            imgList.add(new ImgUploadItem());
        }
        return imgList;
    }

    @Override
    public List<ImgUploadItem> getValue() {
        return imgList;
    }
}
