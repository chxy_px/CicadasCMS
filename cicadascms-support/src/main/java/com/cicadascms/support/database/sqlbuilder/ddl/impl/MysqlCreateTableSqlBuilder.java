/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.database.sqlbuilder.ddl.impl;

import com.cicadascms.common.func.Fn;
import com.cicadascms.support.database.sqlbuilder.AbstractSqlBuilder;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import com.cicadascms.support.database.sqlbuilder.ddl.CreateTableSqlBuilder;

public class MysqlCreateTableSqlBuilder extends AbstractSqlBuilder<MysqlCreateTableSqlBuilder> implements CreateTableSqlBuilder<MysqlCreateTableSqlBuilder> {

    // 创建表 开始
    private final static String CREATE_TABLE_BEGIN = "CREATE TABLE `{table}` (";

    //修改表名称
    private final static String RENAME_TABLE_BEGIN = "RENAME TABLE `{table}` to ";

    // 创建表 结束
    private final static String CREATE_TABLE_END = ") ";

    @Override
    public MysqlCreateTableSqlBuilder initColumn(String columnName, String columnType, Integer length, boolean autoIncrement, String defaultValue, boolean isNotNull, boolean isPrimaryKey) {
        MysqlColumnTypeEnum columnTypeEnum = MysqlColumnTypeEnum.checkAndGet(columnType);
        StringBuilder sqlBody = new StringBuilder();
        if (columnTypeEnum.getIsNotLength()) {
            //判断字段是否允许设置长度
            sqlBody.append(" `").append(columnName).append("` ").append(columnType).append("(").append(length).append(")");
        } else {
            sqlBody.append(" `").append(columnName).append("` ").append(columnType);
        }
        if (isNotNull) {
            //判断字段是否不为空
            sqlBody.append(" NOT null");
        }
        if (!autoIncrement) {
            if (!isNotNull) {
                //判断字段是否允许为空
                sqlBody.append(" NULL ");
            }
            if (Fn.isNotNull(defaultValue)) {
                sqlBody.append(" DEFAULT ").append(Fn.isEmpty(defaultValue) ? null : "'" + defaultValue + "'");
            }
        } else {
            // 判断字段类型是否支持自动增长
            if (columnTypeEnum.getAutoIncrement()) {
                if (!isNotNull) {
                    sqlBody.append(" NOT null");
                }
                sqlBody.append(" AUTO_INCREMENT");
            }
        }
        if (isPrimaryKey) {
            //是否为主键
            sqlBody.append(" , PRIMARY KEY (`").append(columnName).append("`) ");
        }
        if (Fn.isNotEmpty(getSqlBody())) {
            sqlBody.append(",")
                    .append("\n")
                    .append(sqlBody.toString());
        }

        setSqlBody(sqlBody.toString());
        return this;
    }

    @Override
    public MysqlCreateTableSqlBuilder rename(String newTableName) {
        setSqlHead(RENAME_TABLE_BEGIN);
        setSqlBody("`" + newTableName + "`");
        setSqlFoot("");
        return this;
    }


    @Override
    public String build() {
        if (getSqlBody().endsWith(",")) {
            setSqlBody(getSqlBody().substring(0, getSqlBody().length() - 1));
        }
        return super.build();
    }

    @Override
    public MysqlCreateTableSqlBuilder tableName(String tableName) {
        clearSql();
        setTableName(tableName);
        setSqlHead(CREATE_TABLE_BEGIN);
        setSqlBody("");
        setSqlFoot(CREATE_TABLE_END);
        return this;
    }

    @Override
    public String buildSql() {
        return build();
    }
}
