/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.database.sqlbuilder;

import com.cicadascms.support.database.sqlbuilder.ddl.AlterTableSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.ddl.CreateTableSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.ddl.DropTableSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.ddl.impl.MysqlAlterTableSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.ddl.impl.MysqlCreateTableSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.ddl.impl.MysqlDropTableSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.dml.DeleteDataSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.dml.InsertDataSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.dml.UpdateDateSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.dml.impl.MysqlDeleteDataSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.dml.impl.MysqlInsertDataSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.dml.impl.MysqlUpdateDataSqlBuilder;

/**
 * SqlBuilder
 *
 * @author Jin
 */
public class SqlBuilder {

    private SqlBuilder() {
    }

    public static SqlBuilder newBuilder() {
        return new SqlBuilder();
    }

    public AlterTableSqlBuilder newAlterTableSqlBuilder() {
        return new MysqlAlterTableSqlBuilder();
    }

    public CreateTableSqlBuilder newCreateTableSqlBuilder() {
        return new MysqlCreateTableSqlBuilder();
    }

    public DropTableSqlBuilder newDropTableSqlBuilder() {
        return new MysqlDropTableSqlBuilder();
    }

    public DeleteDataSqlBuilder newDeleteDataSqlBuilder() {
        return new MysqlDeleteDataSqlBuilder();
    }

    public InsertDataSqlBuilder newInsertDataSqlBuilder() {
        return new MysqlInsertDataSqlBuilder();
    }

    public UpdateDateSqlBuilder newUpdateDateSqlBuilder() {
        return new MysqlUpdateDataSqlBuilder();
    }


}
