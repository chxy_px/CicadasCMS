/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.database.sqlbuilder.ddl;


import com.cicadascms.support.database.sqlbuilder.ISqlBuilder;

/**
 * @author Jin
 * @date 2021-01-31 19:11:43
 * @description: 删除数据表
 */
public interface DropTableSqlBuilder<T extends DropTableSqlBuilder> extends ISqlBuilder {

    T drop();

    T tableName(String tableName);

}
