/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.database.modelfield.prop;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.rule.FileUploadRule;
import com.cicadascms.support.database.modelfield.rule.ImgUploadRule;
import com.cicadascms.support.database.modelfield.value.FileUploadValue;
import com.cicadascms.support.database.modelfield.value.ImgUploadValue;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImgUploadProp implements ModelFieldProp<ImgUploadRule, ImgUploadValue> {
    private String name = ModelFieldTypeEnum.IMG_UPLOAD_NAME;
    private String columnType = MysqlColumnTypeEnum.VARCHAR.getCode();
    private Integer length = 256;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;

    private ImgUploadRule rule = new ImgUploadRule();
    private ImgUploadValue value = new ImgUploadValue();
}
