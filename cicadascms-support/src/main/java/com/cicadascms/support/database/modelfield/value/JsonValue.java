/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.database.modelfield.value;

import com.cicadascms.common.func.Fn;
import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * JsonValue
 *
 * @author Jin
 */
@Getter
@Setter
public class JsonValue implements ModelFieldValue<List<Map<String, Object>>> {
    private String type = ModelFieldTypeEnum.JSON_NAME;
    private List<Map<String, Object>> jsonList = new ArrayList<>();

    public List<Map<String, Object>> getJsonList() {
        if (Fn.isEmpty(jsonList)) {
            Map<String, Object> demo = new HashMap<>();
            demo.put("key", "value");
            jsonList.add(demo);
        }
        return jsonList;
    }

    @Override
    public List<Map<String, Object>> getValue() {
        return jsonList;
    }
}
