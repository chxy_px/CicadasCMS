/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.database.sqlbuilder.ddl;

import com.cicadascms.support.database.sqlbuilder.ISqlBuilder;

/**
 * @author Jin
 * @date 2021-01-31 19:11:43
 * @description: 修改数据包
 */
public interface AlterTableSqlBuilder<T extends AlterTableSqlBuilder> extends ISqlBuilder {

    T changeColumn(String columnName, String newColumnName, String columnType, Integer length, boolean autoIncrement, String defaultValue, boolean isNotNull);

    T addColumn(String columnName, String columnType, Integer length, boolean autoIncrement, String defaultValue, boolean isNotNull, boolean isPrimaryKey);

    T dropColumn(String columnName, boolean isPrimaryKey);

    T tableName(String tableName);

}
