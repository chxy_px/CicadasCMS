/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.database.enums;

import com.cicadascms.common.base.BaseEnum;
import com.cicadascms.common.exception.ServiceException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum OracleColumnTypeEnum implements BaseEnum<String> {
    INT("NUMBER(10,0)", false, false, false),
    TNY_INT("NUMBER(10,0)", false, false, false),
    BIG_INT("NUMBER(20,0)", false, false, false),
    SMALL_INT("NUMBER(5,0)", false, false, false),
    MEDIUM_INT("NUMBER(7,0)", false, false, false),

    /* 小数 */
    DECIMAL("DECIMAL", false, false, false),

    /*日期*/
    DATE("DATE", false, false, false),
    DATE_TIME("DATE", false, false, false),
    TIMESTAMP("TIMESTAMP", false, false, false),

    /* 文本 */
    CHAR("CHAR", false, true, false),
    VARCHAR("VARCHAR2", false, true, false),
    TEXT("CLOB", false, true, false),
    MEDIUM_TEXT("CLOB", false, true, false),
    LONG_TEXT("CLOB", false, true, false);

    private final String value;

    private final Boolean autoIncrement;
    private final Boolean isCharTextFiled;
    private final Boolean isNotDefaultValue;

    @Override
    public String toString() {
        return value;
    }

    public static MysqlColumnTypeEnum checkAndGet(BaseEnum columnTypeEnum) {
        return Arrays.stream(MysqlColumnTypeEnum.values()).filter(e -> e.getCode().equals(columnTypeEnum.getCode())).findFirst().orElseThrow(new ServiceException("没有找到合适的字段类型！"));
    }

    @Override
    public String getCode() {
        return value;
    }
}
