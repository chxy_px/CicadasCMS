/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.database.modelfield.prop;


import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.rule.CheckboxRule;
import com.cicadascms.support.database.modelfield.rule.DateTimeRule;
import com.cicadascms.support.database.modelfield.value.CheckboxValue;
import com.cicadascms.support.database.modelfield.value.DateTimeValue;

import lombok.Getter;
import lombok.Setter;

/**
 * DateTimeType
 *
 * @author Jin
 */
@Getter
@Setter
public class DateTimeProp implements ModelFieldProp<DateTimeRule, DateTimeValue> {
    private String name = ModelFieldTypeEnum.DATE_TIME_NAME;
    private String columnType = MysqlColumnTypeEnum.DATE_TIME.getCode();
    private Integer length = 64;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;

    private DateTimeRule rule = new DateTimeRule();
    private DateTimeValue value = new DateTimeValue();

}
