/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.database.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * DBTypeEnum
 *
 * @author Jin
 */
@Getter
@AllArgsConstructor
public enum DBTypeEnum {

    DATABASE_TYPE_H2("h2"),
    DATABASE_TYPE_HSQL("hsql"),
    DATABASE_TYPE_MYSQL("mysql"),
    DATABASE_TYPE_ORACLE("oracle"),
    DATABASE_TYPE_POSTGRES("postgres"),
    DATABASE_TYPE_MSSQL("mssql"),
    DATABASE_TYPE_DB2("db2");

    private final String value;

    @Override
    public String toString() {
        return value;
    }
}
