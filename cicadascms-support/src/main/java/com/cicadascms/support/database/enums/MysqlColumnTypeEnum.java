/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.database.enums;

import com.cicadascms.common.base.BaseEnum;
import com.cicadascms.common.exception.ServiceException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum MysqlColumnTypeEnum implements BaseEnum<String> {
    INT("int", true, false, false),
    TNY_INT("tinyint", true, false, false),
    BIG_INT("bigint", true, false, false),
    SMALL_INT("smallint", true, false, false),
    MEDIUM_INT("mediumint", true, false, false),

    /* 小数 */
    DECIMAL("decimal", false, true, false),

    /*日期*/
    DATE("date", false, true, false),
    DATE_TIME("date", false, true, false),
    TIMESTAMP("timestamp", false, true, false),

    /* 文本 */
    CHAR("char", false, false, false),
    VARCHAR("varchar", false, false, false),
    TEXT("text", false, true, true),
    MEDIUM_TEXT("mediumText", false, true, true),
    LONG_TEXT("longText", false, true, true);

    private final String value;

    private final Boolean autoIncrement;
    private final Boolean isNotLength;
    private final Boolean isNotDefaultValue;

    @Override
    public String toString() {
        return value;
    }

    public static MysqlColumnTypeEnum checkAndGet(String columnType) {
        return Arrays.stream(MysqlColumnTypeEnum.values()).filter(e -> e.getCode().equals(columnType)).findFirst().orElseThrow(new ServiceException("没有找到合适的字段类型！"));
    }

    @Override
    public String getCode() {
        return value;
    }
}
