/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.filter;

import com.cicadascms.common.func.Fn;
import com.cicadascms.support.xss.XssRequestBodyWrapper;
import com.cicadascms.support.xss.XssRequestWrapper;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * GlobalXssFilter
 *
 * @author Jin
 */
@Order(0)
@Component
@WebFilter(filterName = "XssFilter", urlPatterns = {"/*"})
public class GlobalXssFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        if ((Fn.equal(httpServletRequest.getMethod(), "POST") || Fn.equal(httpServletRequest.getMethod(), "PUT")) && Fn.notEqual(
                MediaType.APPLICATION_FORM_URLENCODED_VALUE, httpServletRequest.getContentType())) {
            chain.doFilter(new XssRequestBodyWrapper(httpServletRequest), httpServletResponse);
        } else {
            chain.doFilter(new XssRequestWrapper(httpServletRequest), httpServletResponse);
        }
    }

}
