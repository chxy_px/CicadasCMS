/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.initializer.freemaker;

import cn.hutool.extra.template.TemplateException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.support.annotation.FreeMakerDirectiveRegister;
import freemarker.core.InvalidReferenceException;
import freemarker.core.UnexpectedTypeException;
import freemarker.ext.beans.InvalidPropertyException;
import freemarker.template.Configuration;
import freemarker.template.TemplateModelException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * FreeMaker 指令注册
 *
 * @author Jin
 */
@Slf4j
@Component
public class FreeMarkerInitializer implements CommandLineRunner {

    private Configuration configuration;

    public void setSharedVariable() throws TemplateException {
        Map<String, Object> directiveBeans = SpringContextUtils.getBeansWithAnnotation(FreeMakerDirectiveRegister.class);
        directiveBeans.forEach((name, bean) -> {
            try {
                FreeMakerDirectiveRegister freeMakerDirectiveRegister = AopUtils.getTargetClass(bean).getAnnotation(FreeMakerDirectiveRegister.class);
                if (Fn.isNotNull(freeMakerDirectiveRegister) && Fn.isNotEmpty(freeMakerDirectiveRegister.value())) {
                    log.info("注册FreeMaker指令:{} ", freeMakerDirectiveRegister.value());
                    configuration.setSharedVariable(freeMakerDirectiveRegister.value(), bean);
                } else {
                    log.info("注册FreeMaker指令:{} ", name);
                    configuration.setSharedVariable(name, bean);
                }
            } catch (TemplateModelException e) {
                e.printStackTrace();
                log.error("注册FreeMaker指令:{} 失败！ -{}", name, e);
            }
        });
    }

    public void setTemplateExceptionHandler() {
        configuration.setTemplateExceptionHandler((te, env, out) -> {
            try {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer
                        .append("<p>====================================================================================</p>")
                        .append("<p style=\"font-size:14px;padding-left:10px;\"><b>%s</b></p>")
                        .append("<p style=\"font-size:12px;padding-left:10px;\">错误描述 : " + te.getMessage() + "</p>")
                        .append("<p style=\"font-size:12px;padding-left:10px;\">错误位置 : " + te.getTemplateSourceName() + " - " + te.getLineNumber() + "行 " + te.getColumnNumber() + "列</p>")
                        .append("<p>====================================================================================</p>");
                if (te instanceof InvalidReferenceException) {
                    out.write(String.format(stringBuffer.toString(), "无效引用"));
                } else if (te instanceof UnexpectedTypeException) {
                    out.write(String.format(stringBuffer.toString(), "参数有误"));
                } else if (te instanceof InvalidPropertyException) {
                    out.write(String.format(stringBuffer.toString(), "无效的属性"));
                } else if (te instanceof TemplateModelException) {
                    out.write(String.format(stringBuffer.toString(), "模板模型错误"));
                } else {
                    out.write(String.format(stringBuffer.toString(), "模板渲染异常"));
                }
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void run(String... args) {
        log.info("========[ 注册FreeMaker指令... ]========");
        setSharedVariable();
        setTemplateExceptionHandler();
        log.info("========[ 注册FreeMaker指令完成 ]========\n");
    }

    @Autowired
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }
}

