/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.config.webmvc;

import com.cicadascms.common.utils.ResUtils;
import com.cicadascms.launcher.detector.WebDirDetector;
import com.cicadascms.support.method.CurrentUserHandlerMethodArgumentResolver;
import com.cicadascms.support.method.CurrentWebSiteHandlerMethodArgumentResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.*;

import java.util.List;

/**
 * WebMvcConfig
 *
 * @author Jin
 */
@Slf4j
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    private static final String STATIC_FOLDER = "static";

    @Value("${cicadascms.cors.pathPattern:/**}")
    private String pathPattern;
    @Value("${cicadascms.cors.allowedHeaders:*}")
    private String allowedHeaders;
    @Value("${cicadascms.cors.allowedOrigins:*}")
    private String allowedOrigins;
    @Value("${cicadascms.cors.allowedMethods:*}")
    private String allowedMethods;
    @Value("${cicadascms.cors.allowCredentials:false}")
    private Boolean allowCredentials;
    @Value("${cicadascms.cors.maxAge:3600}")
    private Long maxAge;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //swagger映射
        registry.addResourceHandler("/api-docs/**").addResourceLocations("classpath:/META-INF/resources/");
        //忽略IDE启动项目
        if (WebDirDetector.isStartupFromJar() || WebDirDetector.isStartupFromContainer()) {
            String staticPath = ResUtils.checkAndCreateResourceDir(STATIC_FOLDER);
            log.info("========[ JAR方式运行设置资源路径至 - {} ]========", staticPath);
            registry.addResourceHandler("/static/**").addResourceLocations("file:" + staticPath + "/");
        } else {
            registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        }
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new CurrentUserHandlerMethodArgumentResolver());
        argumentResolvers.add(new CurrentWebSiteHandlerMethodArgumentResolver());
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping(pathPattern)
                .allowedOrigins(allowedOrigins)
                .allowedMethods(allowedMethods)
                .allowCredentials(allowCredentials)
                .maxAge(maxAge)
                .allowedHeaders(allowedHeaders);
    }
}
