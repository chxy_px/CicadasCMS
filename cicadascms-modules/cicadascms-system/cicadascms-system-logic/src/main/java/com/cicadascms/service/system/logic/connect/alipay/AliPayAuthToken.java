package com.cicadascms.service.system.logic.connect.alipay;


import com.cicadascms.security.provider.AbstractConnectAuthToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class AliPayAuthToken extends AbstractConnectAuthToken {

    public AliPayAuthToken(String principal) {
        super(principal);
    }

    public AliPayAuthToken(String principal, boolean loginFailCreate) {
        super(principal, loginFailCreate);
    }

    public AliPayAuthToken(Collection<? extends GrantedAuthority> authorities, Object principal, boolean loginFailCreate) {
        super(authorities, principal, loginFailCreate);
    }

    public AliPayAuthToken(Object principal, Collection<? extends GrantedAuthority> authorities) {
        super(principal, authorities);
    }
}
