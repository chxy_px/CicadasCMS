package com.cicadascms.service.system.logic.vo;


import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "DictDetailsVo", description = "DictDetailsVo")
public class DictDetailsVo implements Serializable{
    private String name;
    private Integer value;

}
