package com.cicadascms.service.system.logic.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.RoleDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * RoleQueryDTO对象
 * 角色表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="RoleQueryDTO对象")
public class RoleQueryDTO extends BaseDTO<RoleQueryDTO, RoleDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-降序排序字段 多个字段用英文逗号隔开")
    private String descs;

    @ApiModelProperty(value = "4-升序排序字段  多个字段用英文逗号隔开")
    private String ascs;

    /**
    * 角色id
    */
    @ApiModelProperty(value = "2-角色id" )
    private Integer roleId;
    /**
    * 租户id
    */
    @ApiModelProperty(value = "3-租户id" )
    private Integer tenantId;
    /**
    * 父编号
    */
    @ApiModelProperty(value = "4-父编号" )
    private Integer parentId;
    /**
    * 角色名称
    */
    @ApiModelProperty(value = "5-角色名称" )
    private String roleName;
    /**
    * 角色标识
    */
    @ApiModelProperty(value = "6-角色标识" )
    private String roleKey;
    /**
    * 菜单类型(1，系统角色，2，应用角色)
    */
    @ApiModelProperty(value = "7-菜单类型(1，系统角色，2，应用角色)" )
    private Integer roleType;
    /**
    * 说明
    */
    @ApiModelProperty(value = "8-说明" )
    private String remark;

    public Page<RoleDO> page() {
        Page<RoleDO>  page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<RoleQueryDTO, RoleDO> converter = new Converter<RoleQueryDTO, RoleDO>() {
        @Override
        public RoleDO doForward(RoleQueryDTO roleQueryDTO) {
            return WarpsUtils.copyTo(roleQueryDTO, RoleDO.class);
        }

        @Override
        public RoleQueryDTO doBackward(RoleDO role) {
            return WarpsUtils.copyTo(role, RoleQueryDTO.class);
        }
    };

    @Override
    public RoleDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public RoleQueryDTO convertFor(RoleDO role) {
        return converter.doBackward(role);
    }
}
