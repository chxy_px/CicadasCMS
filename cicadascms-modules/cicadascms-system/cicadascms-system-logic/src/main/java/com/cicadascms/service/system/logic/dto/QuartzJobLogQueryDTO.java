package com.cicadascms.service.system.logic.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.QuartzJobLogDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * QuartzJobQueryDTO对象
 * 定时任务
 * </p>
 *
 * @author jin
 * @since 2020-04-29
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "QuartzJobQueryDTO对象")
public class QuartzJobLogQueryDTO extends BaseDTO<QuartzJobLogQueryDTO, QuartzJobLogDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-降序排序字段 多个字段用英文逗号隔开")
    private String descs;

    @ApiModelProperty(value = "4-升序排序字段  多个字段用英文逗号隔开")
    private String ascs;

    /**
     * 任务名称
     */
    @NotBlank(message = "任务名称不能为空！")
    @ApiModelProperty(value = "3-任务名称")
    private String jobName;


    public Page<QuartzJobLogDO> page() {
        Page<QuartzJobLogDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<QuartzJobLogQueryDTO, QuartzJobLogDO> converter = new Converter<QuartzJobLogQueryDTO, QuartzJobLogDO>() {
        @Override
        public QuartzJobLogDO doForward(QuartzJobLogQueryDTO quartzJobQueryDTO) {
            return WarpsUtils.copyTo(quartzJobQueryDTO, QuartzJobLogDO.class);
        }

        @Override
        public QuartzJobLogQueryDTO doBackward(QuartzJobLogDO quartzJob) {
            return WarpsUtils.copyTo(quartzJob, QuartzJobLogQueryDTO.class);
        }
    };

    @Override
    public QuartzJobLogDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public QuartzJobLogQueryDTO convertFor(QuartzJobLogDO quartzJob) {
        return converter.doBackward(quartzJob);
    }
}
