package com.cicadascms.service.system.logic.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.QuartzJobDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * QuartzJobQueryDTO对象
 * 定时任务
 * </p>
 *
 * @author jin
 * @since 2020-04-29
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="QuartzJobQueryDTO对象")
public class QuartzJobQueryDTO extends BaseDTO<QuartzJobQueryDTO, QuartzJobDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-降序排序字段 多个字段用英文逗号隔开")
    private String descs;

    @ApiModelProperty(value = "4-升序排序字段  多个字段用英文逗号隔开")
    private String ascs;

    /**
    * 任务名称
    */
    @ApiModelProperty(value = "3-任务名称" )
    private String jobName;
    /**
    * 任务分组
    */
    @ApiModelProperty(value = "4-任务分组" )
    private String jobGroup;
    /**
    * 执行类
    */
    @ApiModelProperty(value = "5-执行类" )
    private String jobClassName;
    /**
    * cron表达式
    */
    @ApiModelProperty(value = "6-cron表达式" )
    private String cronExpression;
    /**
    * 任务状态
    */
    @ApiModelProperty(value = "7-任务状态" )
    private String triggerState;
    /**
    * 修改之前的任务名称
    */
    @ApiModelProperty(value = "8-修改之前的任务名称" )
    private String oldJobName;
    /**
    * 修改之前的任务分组
    */
    @ApiModelProperty(value = "9-修改之前的任务分组" )
    private String oldJobGroup;
    /**
    * 描述
    */
    @ApiModelProperty(value = "10-描述" )
    private String description;

    public Page<QuartzJobDO> page() {
        Page<QuartzJobDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<QuartzJobQueryDTO, QuartzJobDO> converter = new Converter<QuartzJobQueryDTO, QuartzJobDO>() {
        @Override
        public QuartzJobDO doForward(QuartzJobQueryDTO quartzJobQueryDTO) {
            return WarpsUtils.copyTo(quartzJobQueryDTO, QuartzJobDO.class);
        }

        @Override
        public QuartzJobQueryDTO doBackward(QuartzJobDO quartzJob) {
            return WarpsUtils.copyTo(quartzJob, QuartzJobQueryDTO.class);
        }
    };

    @Override
    public QuartzJobDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public QuartzJobQueryDTO convertFor(QuartzJobDO quartzJob) {
        return converter.doBackward(quartzJob);
    }
}
