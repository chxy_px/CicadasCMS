package com.cicadascms.service.system.logic.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.service.system.logic.dto.PropertyInputDTO;
import com.cicadascms.service.system.logic.dto.PropertyQueryDTO;
import com.cicadascms.service.system.logic.dto.PropertyUpdateDTO;
import com.cicadascms.service.system.logic.service.IPropertyService;
import com.cicadascms.service.system.logic.vo.PropertyVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 *  控制器
 * </p>
 *
 * @author Jin
 * @since 2021-06-06
 */
@Api(tags = "参数管理接口")
@RestController
@RequestMapping("/system/property")
@AllArgsConstructor
public class PropertyController {
    private final IPropertyService propertyService;

    @ApiOperation(value = "分页接口")
    @GetMapping("/page")
    public R<Page<PropertyVO>> page(PropertyQueryDTO propertyQueryDTO) {
        return propertyService.page(propertyQueryDTO);
    }

    @ApiOperation(value = "保存接口")
    @PostMapping
    public R<Boolean> save(@Valid @RequestBody PropertyInputDTO propertyInputDTO) {
        return propertyService.save(propertyInputDTO);
    }

    @ApiOperation(value = "更新接口")
    @PutMapping
    public R<Boolean> updateById(@Valid @RequestBody PropertyUpdateDTO propertyUpdateDTO) {
        return propertyService.update(propertyUpdateDTO);
    }

    @ApiOperation(value = "详情接口")
    @GetMapping("/{id}")
    public R<PropertyVO> getById(@PathVariable Long id) {
        return propertyService.findById(id);
    }

    @ApiOperation(value = "删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return propertyService.deleteById(id);
    }


}
