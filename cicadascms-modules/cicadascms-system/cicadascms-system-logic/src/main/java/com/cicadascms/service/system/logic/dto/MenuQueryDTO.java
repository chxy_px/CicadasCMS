package com.cicadascms.service.system.logic.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.MenuDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * MenuQueryDTO对象
 * 系统菜单表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "MenuQueryDTO对象")
public class MenuQueryDTO extends BaseDTO<MenuQueryDTO, MenuDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 父菜单编号
     */
    @ApiModelProperty(value = "父菜单编号")
    private Integer parentId;
    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String menuName;
    /**
     * 菜单类型（1,一级菜单，2，二级菜单，3,按钮）
     */
    @ApiModelProperty(value = "菜单类型（1,一级菜单，2，二级菜单，3,按钮）")
    private Integer menuType;


    public static Converter<MenuQueryDTO, MenuDO> converter = new Converter<MenuQueryDTO, MenuDO>() {
        @Override
        public MenuDO doForward(MenuQueryDTO menuQueryDTO) {
            return WarpsUtils.copyTo(menuQueryDTO, MenuDO.class);
        }

        @Override
        public MenuQueryDTO doBackward(MenuDO menu) {
            return WarpsUtils.copyTo(menu, MenuQueryDTO.class);
        }
    };

    @Override
    public MenuDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public MenuQueryDTO convertFor(MenuDO menu) {
        return converter.doBackward(menu);
    }
}
