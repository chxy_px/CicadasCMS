package com.cicadascms.service.system.logic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Jin
 * @date 2020-08-25 14:11:36
 * @description: TODO
 */
@Getter
@Setter
@ToString
public class TokenVO {
    @ApiModelProperty(value = "令牌类型")
    private String tokenType;
    @ApiModelProperty(value = "令牌值")
    private String tokenValue;
    @ApiModelProperty(value = "过期时间")
    private Integer expiresIn;
    @ApiModelProperty(value = "应用编号")
    private String clientId;
    @ApiModelProperty(value = "授权类型")
    private String grantType;
    @ApiModelProperty(value = "用户编号")
    private Integer uid;
    @ApiModelProperty(value = "用户名称")
    private String username;
    @ApiModelProperty(value = "头像")
    private String avatar;
}
