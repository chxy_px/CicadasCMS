package com.cicadascms.service.system.logic.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.cicadascms.common.resp.R;
import com.cicadascms.service.system.logic.dto.AttrInputDTO;
import com.cicadascms.service.system.logic.dto.AttrQueryDTO;
import com.cicadascms.service.system.logic.dto.AttrUpdateDTO;
import com.cicadascms.service.system.logic.service.IAttrService;
import com.cicadascms.service.system.logic.vo.AttrVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 * 附件 控制器
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Api(tags = "附件接口")
@RestController
@RequestMapping("/system/attr")
@AllArgsConstructor
public class AttrController {
    private final IAttrService attrService;

    @ApiOperation(value = "附件分页接口")
    @GetMapping("/list")
    public R<Page<AttrVO>> page(AttrQueryDTO attrQueryDTO) {
        return attrService.page(attrQueryDTO);
    }

    @ApiOperation(value = "附件保存接口")
    @PostMapping
    public R<Boolean> save(@Valid AttrInputDTO attrInputDTO) {
        return attrService.save(attrInputDTO);
    }

    @ApiOperation(value = "附件更新接口")
    @PutMapping
    public R<Boolean> updateById(@Valid AttrUpdateDTO attrUpdateDTO) {
        return attrService.update(attrUpdateDTO);
    }

    @ApiOperation(value = "附件详情接口")
    @GetMapping("/{id}")
    public R<AttrVO> getById(@PathVariable Long id) {
        return attrService.findById(id);
    }

    @ApiOperation(value = "附件删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return attrService.deleteById(id);
    }


}
