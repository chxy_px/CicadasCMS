package com.cicadascms.service.system.logic.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.PropertyDO;
import com.cicadascms.service.system.logic.dto.PropertyInputDTO;
import com.cicadascms.service.system.logic.dto.PropertyQueryDTO;
import com.cicadascms.service.system.logic.dto.PropertyUpdateDTO;

import java.io.Serializable;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Jin
 * @since 2021-06-06
 */
public interface IPropertyService extends IService<PropertyDO> {

    /**
     * 分页方法
     * @param propertyQueryDTO
     * @return
     */
    R page(PropertyQueryDTO propertyQueryDTO);

    /**
     * 保存方法
     * @param propertyInputDTO
     * @return
     */
    R save(PropertyInputDTO propertyInputDTO);

    /**
     * 更新方法
     * @param propertyUpdateDTO
     * @return
     */
    R update(PropertyUpdateDTO propertyUpdateDTO);

    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);
}
