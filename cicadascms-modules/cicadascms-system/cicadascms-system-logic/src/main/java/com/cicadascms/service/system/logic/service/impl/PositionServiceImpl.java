package com.cicadascms.service.system.logic.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.PositionDO;
import com.cicadascms.data.mapper.SysPositionMapper;
import com.cicadascms.service.system.logic.dto.PositionInputDTO;
import com.cicadascms.service.system.logic.dto.PositionQueryDTO;
import com.cicadascms.service.system.logic.dto.PositionUpdateDTO;
import com.cicadascms.service.system.logic.service.IPositionService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 职位表 服务实现类
 * </p>
 *
 * @author jin
 * @since 2020-08-25
 */
@Service("PositionServiceImpl")
public class PositionServiceImpl extends BaseService<SysPositionMapper, PositionDO> implements IPositionService {

    @Override
    public List<PositionDO> findByUserId(Serializable userId) {
        return baseMapper.selectByUserId(userId);
    }

    @Override
    public R page(PositionQueryDTO positionQueryDTO) {
        LambdaQueryWrapper<PositionDO> lambdaQueryWrapper = getLambdaQueryWrapper().orderByAsc(PositionDO::getSortId);
        if (Fn.isNotEmpty(positionQueryDTO.getPostName())) {
            lambdaQueryWrapper.like(PositionDO::getPostName, positionQueryDTO.getPostName());
        }
        return R.ok(baseMapper.selectPage(positionQueryDTO.page(), lambdaQueryWrapper));
    }

    @Override
    public R save(PositionInputDTO positionInputDTO) {
        PositionDO position = positionInputDTO.convertToEntity();
        baseMapper.insert(position);
        return R.ok(true);
    }

    @Override
    public R update(PositionUpdateDTO positionUpdateDTO) {
        PositionDO position = positionUpdateDTO.convertToEntity();
        baseMapper.updateById(position);
        return R.ok(true);
    }

    @Override
    public R findById(Serializable id) {
        PositionDO position = baseMapper.selectById(id);
        return R.ok(position);
    }

    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return R.ok(true);
    }

}
