package com.cicadascms.service.system.logic.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * AttrVO对象
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AttrVO对象", description="附件")
public class AttrVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
    * 附件分类
    */
    @ApiModelProperty(value = "2-附件分类" )
    private Integer attrClassId;
    /**
    * 附件名称
    */
    @ApiModelProperty(value = "3-附件名称" )
    private String attrName;
    /**
    * 附件存储类型
    */
    @ApiModelProperty(value = "4-附件存储类型" )
    private Integer attrStoreType;
    /**
    * 附件地址
    */
    @ApiModelProperty(value = "5-附件地址" )
    private String attrLocation;
    /**
    * 创建人
    */
    @ApiModelProperty(value = "6-创建人" )
    private Integer createBy;
    /**
    * 创建时间
    */
    @ApiModelProperty(value = "7-创建时间" )
    private LocalDateTime createTime;

}
