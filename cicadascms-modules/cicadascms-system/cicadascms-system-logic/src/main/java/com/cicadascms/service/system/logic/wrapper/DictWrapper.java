package com.cicadascms.service.system.logic.wrapper;


import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.dict.Dict;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.DictDO;
import com.cicadascms.service.system.logic.service.IDictService;
import com.cicadascms.service.system.logic.vo.DictVO;

import java.util.List;

public class DictWrapper implements BaseWrapper<DictDO, Dict> {

    private final static IDictService dictService;

    static {
        dictService = SpringContextUtils.getBean(IDictService.class);
    }

    public static DictWrapper newBuilder() {
        return new DictWrapper();
    }

    @Override
    public DictVO entityVO(DictDO entity) {
        DictVO entityVo = WarpsUtils.copyTo(entity, DictVO.class);
        List<DictDO> childList = dictService.findByParentId(entity.getId());
        assert entityVo != null;
        entityVo.setChildren(listVO(childList));
        if (Fn.isNotNull(entityVo.getParentId()) && Fn.notEqual(entityVo.getParentId(), Constant.PARENT_ID)) {
            DictDO parentDict = dictService.getById(entityVo.getParentId());
            entityVo.setParentName(parentDict.getDictName());
        }
        return entityVo;
    }

}
