package com.cicadascms.service.front.logic.service.impl;

import com.cicadascms.common.base.BaseService;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.data.mapper.SiteMapper;
import com.cicadascms.service.front.logic.service.ISiteService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 站点表 服务实现类
 * </p>
 *
 * @author jin
 * @since 2020-10-10
 */
@Service
public class SiteServiceImpl extends BaseService<SiteMapper, SiteDO> implements ISiteService {

    @Override
    public SiteDO findByDomain(String domain) {
        return super.getOne(getLambdaQueryWrapper().eq(SiteDO::getDomain, domain));
    }

    @Override
    public SiteDO findBySiteDir(String dir) {
        return super.getOne(getLambdaQueryWrapper().eq(SiteDO::getSiteDir, dir));
    }

    @Override
    public SiteDO getDefaultSite() {
        return super.getOne(getLambdaQueryWrapper().eq(SiteDO::getIsDefault, true));
    }

}
