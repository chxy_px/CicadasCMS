package com.cicadascms.service.front.logic.controller.web;

import com.cicadascms.common.exception.FrontNotFoundException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.ContentDO;
import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.service.front.logic.service.IChannelService;
import com.cicadascms.service.front.logic.service.IContentService;
import com.cicadascms.service.front.logic.service.IModelService;
import com.cicadascms.service.front.logic.vo.ChannelVO;
import com.cicadascms.service.front.logic.wrapper.ContentWrapper;
import com.cicadascms.support.annotation.CurrentWebSite;
import com.cicadascms.support.website.WebSiteViewUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ContentController {
    private IChannelService channelService;
    private IContentService contentService;
    private IModelService modelService;

    @RequestMapping("/{channelUrlPath}/{contentId:\\d+}")
    public String index(@PathVariable String channelUrlPath, @PathVariable Integer contentId, @CurrentWebSite SiteDO siteDO, Model model) {
        return page(channelUrlPath, contentId, 1, siteDO, model);
    }


    @RequestMapping("/{channelUrlPath}/{contentId:\\d+}_{pageNumber:\\d+}")
    public String page(@PathVariable String channelUrlPath, @PathVariable Integer contentId, @PathVariable Integer pageNumber, @CurrentWebSite SiteDO siteDO, Model model) {
        ChannelVO channelVO = channelService.findBySiteIdAndChannelUrlPath(siteDO.getSiteId(), channelUrlPath);
        if (Fn.isNull(channelVO)) {
            throw new FrontNotFoundException("栏目已被删除或不存在！");
        }
        model.addAttribute("channel", channelVO);

        ContentDO contentDO = contentService.getById(contentId);
        if (Fn.isNull(contentDO)) {
            throw new FrontNotFoundException("内容已被删除或不存在！");
        }

        ModelDO modelDO = modelService.getById(contentDO.getModelId());
        if (Fn.isNull(modelDO)) {
            throw new FrontNotFoundException("内容模型被删除或已经禁用！");
        }
        model.addAttribute("content", ContentWrapper.newBuilder().entityVO(contentDO));
        model.addAttribute("site", siteDO);
        return WebSiteViewUtil.contentViewRender(siteDO.getPcTemplateDir(), siteDO.getMobileTemplateDir(), modelDO.getModelView());
    }

    @Autowired
    public void setContentService(IContentService contentService) {
        this.contentService = contentService;
    }

    @Autowired
    public void setChannelService(IChannelService channelService) {
        this.channelService = channelService;
    }

    @Autowired
    public void setModelService(IModelService modelService) {
        this.modelService = modelService;
    }
}
