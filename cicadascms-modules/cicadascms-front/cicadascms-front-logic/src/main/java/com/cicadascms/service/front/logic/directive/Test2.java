package com.cicadascms.service.front.logic.directive;

import com.cicadascms.support.annotation.FreeMakerDirectiveRegister;
import com.cicadascms.support.freemaker.directive.AbstractTemplateDirectiveModel;
import freemarker.template.TemplateException;

@FreeMakerDirectiveRegister("Test2")
public class Test2 extends AbstractTemplateDirectiveModel {

    @Override
    protected void render() throws TemplateException {
        System.out.println("TEST------------------2");
        System.out.println("TEST------------------2");
        System.out.println("TEST------------------2");
        System.out.println(getInt("A"));
        System.out.println(this.toString());

    }

}
