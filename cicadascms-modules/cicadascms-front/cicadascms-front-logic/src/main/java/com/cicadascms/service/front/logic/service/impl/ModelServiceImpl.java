package com.cicadascms.service.front.logic.service.impl;

import com.cicadascms.common.base.BaseService;
import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.data.mapper.ModelMapper;
import com.cicadascms.service.front.logic.service.IModelService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

/**
 * <p>
 * 内容模型表 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Service("modelService")
@AllArgsConstructor
public class ModelServiceImpl extends BaseService<ModelMapper, ModelDO> implements IModelService {
    private final DataSource dataSource;

    public static final Integer DEFAULT_CHANNEL_TYPE_CODE = 1;
    public static final String DEFAULT_CHANNEL_ID = "channel_id";
    public static final String DEFAULT_CHANNEL_TABLE_PREFIX = "cms_channel_";

    public static final Integer DEFAULT_CONTENT_TYPE_CODE = 2;
    public static final String DEFAULT_CONTENT_ID = "content_id";
    public static final String DEFAULT_CONTENT_TABLE_PREFIX = "cms_content_";

    public static final Integer DEFAULT_FORM_TYPE_CODE = 3;
    public static final String DEFAULT_FORM_ID = "form_id";
    public static final String DEFAULT_FORM_TABLE_PREFIX = "cms_form_";


}
