package com.cicadascms.service.admin.logic.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ContentDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * ContentUpdateDTO对象
 * 内容
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "ContentUpdateDTO对象")
public class ContentUpdateDTO extends BaseDTO<ContentUpdateDTO, ContentDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 内容编号
     */
    @ApiModelProperty(value = "1-内容编号")
    private Integer contentId;
    /**
     * 站点编号
     */
    @ApiModelProperty(value = "2-站点编号")
    private Integer siteId;
    /**
     * 栏目编号
     */
    @ApiModelProperty(value = "3-栏目编号")
    private Integer channelId;
    /**
     * 模型编号
     */
    @ApiModelProperty(value = "4-模型编号")
    private Integer modelId;
    /**
     * 标题
     */
    @ApiModelProperty(value = "5-标题")
    private String title;
    /**
     * 副标题
     */
    @ApiModelProperty(value = "6-副标题")
    private String subTitle;
    /**
     * 作者
     */
    @ApiModelProperty(value = "7-作者")
    private String author;
    /**
     * 页面关键字
     */
    @ApiModelProperty(value = "8-页面关键字")
    private Integer keywords;
    /**
     * 页面描述
     */
    @ApiModelProperty(value = "9-页面描述")
    private Integer description;
    /**
     * 录入时间
     */
    @ApiModelProperty(value = "10-录入时间")
    private LocalDateTime inputTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "11-更新时间")
    private Integer updateTime;
    /**
     * 内容状态
     */
    @ApiModelProperty(value = "12-内容状态")
    private Integer state;
    /**
     * 来源
     */
    @ApiModelProperty(value = "13-来源")
    private String source;
    /**
     * 原文地址
     */
    @ApiModelProperty(value = "14-原文地址")
    private String sourceUrl;
    /**
     * 封面图片
     */
    @ApiModelProperty(value = "15-封面图片")
    private String thumb;
    /**
     * 浏览数量
     */
    @ApiModelProperty(value = "16-浏览数量")
    private Integer viewNum;
    /**
     * 价格
     */
    @ApiModelProperty(value = "17-价格")
    private Integer price;
    /**
     * 付费阅读
     */
    @ApiModelProperty(value = "18-付费阅读")
    private Boolean paidReading;
    /**
     * 内容分页
     */
    @ApiModelProperty(value = "17-内容分页")
    private Integer pageTotal;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "18-扩展字段")
    private List<ModelFieldValueDTO> ext;

    public static Converter<ContentUpdateDTO, ContentDO> converter = new Converter<ContentUpdateDTO, ContentDO>() {
        @Override
        public ContentDO doForward(ContentUpdateDTO contentUpdateDTO) {
            return WarpsUtils.copyTo(contentUpdateDTO, ContentDO.class);
        }

        @Override
        public ContentUpdateDTO doBackward(ContentDO contentDO) {
            return WarpsUtils.copyTo(contentDO, ContentUpdateDTO.class);
        }
    };

    @Override
    public ContentDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ContentUpdateDTO convertFor(ContentDO contentDO) {
        return converter.doBackward(contentDO);
    }
}
