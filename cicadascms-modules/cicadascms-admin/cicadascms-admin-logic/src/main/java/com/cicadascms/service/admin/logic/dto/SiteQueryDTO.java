package com.cicadascms.service.admin.logic.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.SiteDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * SiteQueryDTO对象
 * 站点表
 * </p>
 *
 * @author jin
 * @since 2020-10-12
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="SiteQueryDTO对象")
public class SiteQueryDTO extends BaseDTO<SiteQueryDTO, SiteDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-排序字段")
    private String descs;

    @ApiModelProperty(value = "4-排序字段")
    private String ascs;

    /**
    * 站点名称
    */
    @ApiModelProperty(value = "3-站点名称" )
    private String siteName;
    /**
    * http协议
    */
    @ApiModelProperty(value = "4-http协议" )
    private Integer httpProtocol;
    /**
    * 站点域名
    */
    @ApiModelProperty(value = "5-站点域名" )
    private String domain;
    /**
    * 站点状态
    */
    @ApiModelProperty(value = "7-站点状态" )
    private Boolean status;

    public Page<SiteDO> page() {
        Page<SiteDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<SiteQueryDTO, SiteDO> converter = new Converter<SiteQueryDTO, SiteDO>() {
        @Override
        public SiteDO doForward(SiteQueryDTO siteQueryDTO) {
            return WarpsUtils.copyTo(siteQueryDTO, SiteDO.class);
        }

        @Override
        public SiteQueryDTO doBackward(SiteDO siteDO) {
            return WarpsUtils.copyTo(siteDO, SiteQueryDTO.class);
        }
    };

    @Override
    public SiteDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public SiteQueryDTO convertFor(SiteDO siteDO) {
        return converter.doBackward(siteDO);
    }
}
