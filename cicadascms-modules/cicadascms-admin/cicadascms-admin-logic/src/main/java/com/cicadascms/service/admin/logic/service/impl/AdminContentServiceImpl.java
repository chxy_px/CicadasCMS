package com.cicadascms.service.admin.logic.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.service.admin.logic.dto.*;
import com.cicadascms.service.admin.logic.vo.ContentVO;
import com.cicadascms.service.admin.logic.wrapper.ContentWrapper;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.lucene.manager.LuceneManager;
import com.cicadascms.service.admin.logic.service.IAdminChannelService;
import com.cicadascms.service.admin.logic.service.IAdminContentService;
import com.cicadascms.service.admin.logic.service.IAdminModelFieldService;
import com.cicadascms.service.admin.logic.service.IAdminModelService;
import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.common.utils.DbUtils;
import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.data.domain.ContentDO;
import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.data.mapper.ContentMapper;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import com.cicadascms.support.database.modelfield.value.EditorValue;
import com.cicadascms.support.database.sqlbuilder.SqlBuilder;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 内容 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Service
@AllArgsConstructor
public class AdminContentServiceImpl extends BaseService<ContentMapper, ContentDO> implements IAdminContentService {
    private final IAdminChannelService channelService;
    private final IAdminModelService modelService;
    private final IAdminModelFieldService modelFieldService;
    private final DataSource dataSource;
    private final LuceneManager luceneManager;
    private final static String DEFAULT_CONTENT_ID = "content_id";

    @SneakyThrows
    @Override
    public R page(ContentQueryDTO contentQueryDTO) {
        ContentDO contentDO = contentQueryDTO.convertToEntity();
        Page page = baseMapper.selectPage(contentQueryDTO.page(), getLambdaQueryWrapper().setEntity(contentDO));
        return R.ok(ContentWrapper.newBuilder().pageVO(page));
    }


    @SneakyThrows
    @Transactional
    @Override
    public R save(ContentInputDTO contentInputDTO) {
        ContentDO contentDO = contentInputDTO.convertToEntity();
        if (!save(contentDO)) return R.error("内容添加失败！", false);
        ContentIndexDTO contentIndexDTO = copyTo(contentDO, ContentIndexDTO.class);
        //是否包含扩展字段
        if (Fn.isNotNull(contentInputDTO.getExt())) {
            ModelDO modelDO = modelService.getById(contentDO.getModelId());
            Map<String, Object> fieldKeyAndValue = new HashMap<>();
            //设置主键
            fieldKeyAndValue.put(DEFAULT_CONTENT_ID, contentDO.getContentId());
            //需要创建索引的字段和值
            Map<String, Object> fieldSearchKeyAndValue = new HashMap<>();
            //遍历填充模型字段
            fillModelFieldKeyAndValue(contentDO, modelDO, fieldKeyAndValue,fieldSearchKeyAndValue, contentInputDTO.getExt());
            //设置Lucene索引扩展字段
            contentIndexDTO.setExt(fieldSearchKeyAndValue);
            //构建插入语句
            String sql = SqlBuilder.newBuilder()
                    .newInsertDataSqlBuilder()
                    .tableName(modelDO.getTableName())
                    .insert(new ArrayList<>(fieldKeyAndValue.keySet()),
                            new ArrayList<>(fieldKeyAndValue.values()))
                    .buildSql();
            DbUtils.use(dataSource).exec(sql);
        }
        //创建lucene索引
        luceneManager.create(contentIndexDTO);
        return R.ok("内容更新成功！", true);
    }

    @SneakyThrows
    @Transactional
    @Override
    public R update(ContentUpdateDTO contentUpdateDTO) {
        ContentDO contentDO = contentUpdateDTO.convertToEntity();
        if (updateById(contentDO)) return R.error("内容更新失败！", false);
        ContentIndexDTO contentIndexDTO = copyTo(contentDO, ContentIndexDTO.class);
        if (Fn.isNotNull(contentUpdateDTO.getExt())) {
            ModelDO modelDO = modelService.getById(contentDO.getModelId());
            Map<String, Object> fieldKeyAndValue = new HashMap<>();
            //需要创建索引的字段和值
            Map<String, Object> fieldSearchKeyAndValue = new HashMap<>();
            //遍历填充模型字段
            fillModelFieldKeyAndValue(contentDO, modelDO, fieldKeyAndValue,fieldSearchKeyAndValue, contentUpdateDTO.getExt());
            //设置Lucene索引扩展字段
            contentIndexDTO.setExt(fieldSearchKeyAndValue);
            //构建更新语句
            String sql = SqlBuilder.newBuilder()
                    .newUpdateDateSqlBuilder()
                    .tableName(modelDO.getTableName())
                    .update(DEFAULT_CONTENT_ID, contentDO.getContentId(),
                            new ArrayList<>(fieldKeyAndValue.keySet()),
                            new ArrayList<>(fieldKeyAndValue.values()))
                    .buildSql();
            DbUtils.use(dataSource).exec(sql);
        }
        //更新lucene索引
        luceneManager.update(contentIndexDTO);
        return R.ok("内容更新成功！", true);
    }

    /**
     * 填充扩展字段
     *
     * @param contentDO
     * @param modelDO
     * @param fieldKeyAndValue
     * @param fieldSearchKeyAndValue
     * @param extModelFields
     */
    private void fillModelFieldKeyAndValue(ContentDO contentDO, ModelDO modelDO, Map<String, Object> fieldKeyAndValue, Map<String, Object> fieldSearchKeyAndValue, List<ModelFieldValueDTO> extModelFields) {
        List<ModelFieldDO> modelFieldDOS = modelFieldService.findByModelId(modelDO.getModelId());
        if (Fn.isEmpty(modelFieldDOS)) throw new ServiceException("模型已被删除或不存在！");
        //遍历填充
        modelFieldDOS.stream().parallel().forEach(modelFieldDO -> {
            //获取模型字段值
            ModelFieldValue modelFieldValue = extModelFields
                    .stream()
                    .filter(e -> Fn.equal(e.getFieldName(), modelFieldDO.getFieldName()))
                    .findFirst()
                    .orElseThrow(new ServiceException("字段名称有误！"))
                    .getFieldValue();
            //有数据填充
            if (Fn.isNotNull(modelFieldValue)) {
                //富文本肯能包含分页
                if (Fn.equal(modelFieldDO.getFieldType(), ModelFieldTypeEnum.富文本编辑器.getCode())) {
                    EditorValue editorValue = (EditorValue) modelFieldValue;
                    //设置内容分页数量
                    if (Fn.isNotEmpty(editorValue.getValue())) {
                        int count = StrUtil.count(editorValue.getValue(), "");
                        contentDO.setPageTotal(count + 1);
                        updateById(contentDO);
                    }
                }
                fieldKeyAndValue.put(modelFieldDO.getFieldName(), Fn.toJson(modelFieldValue));

                //是否检索字段
                if(modelFieldDO.getIsSearchField()){
                    fieldSearchKeyAndValue.put(modelFieldDO.getFieldName(),modelFieldValue.getValue());
                }
            }
        });
    }

    @Override
    public R findById(Serializable id) {
        ContentDO contentDO = baseMapper.selectById(id);
        return R.ok(ContentWrapper.newBuilder().entityVO(contentDO));

    }

    @SneakyThrows
    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        luceneManager.delete(String.valueOf(id));
        return R.ok(true);
    }

    @Override
    public IPage<ContentVO> findPageBySiteIdAndChannelId(int current, int size, Serializable siteId, Serializable channelId) {
        ChannelDO channelDO = channelService.getById(channelId);
        if (Fn.isNull(channelDO)) {
            throw new ServiceException("栏目已被删除或不存在！");
        }
        LambdaQueryWrapper<ContentDO> lambdaQueryWrapper = getLambdaQueryWrapper();
        lambdaQueryWrapper.eq(ContentDO::getSiteId, siteId);
        List<ChannelDO> childChannelDOList = channelService.list(new LambdaQueryWrapper<ChannelDO>().eq(ChannelDO::getParentId, channelId));
        //如果当前栏目存在下级栏目就查询下级栏目的
        if (Fn.isNotEmpty(childChannelDOList)) {
            List<Integer> childChannelIds = childChannelDOList.stream().map(ChannelDO::getChannelId).collect(Collectors.toList());
            lambdaQueryWrapper.in(ContentDO::getChannelId, childChannelIds);
        } else {
            lambdaQueryWrapper.eq(ContentDO::getChannelId, channelId);
        }
        IPage<ContentDO> page = page(new Page<>(current, size), lambdaQueryWrapper);
        return ContentWrapper.newBuilder().pageVO(page);
    }

    @Override
    public Map<String, Object> findByTableNameAndContentId(String fields, String tableName, Integer contentId) {
        return baseMapper.selectByTableNameAndContentId(fields, tableName, contentId);
    }
}
