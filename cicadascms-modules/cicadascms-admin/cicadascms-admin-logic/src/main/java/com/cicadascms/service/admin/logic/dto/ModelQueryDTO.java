package com.cicadascms.service.admin.logic.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ModelDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * ModelQueryDTO对象
 * 内容模型表
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="ModelQueryDTO对象")
public class ModelQueryDTO extends BaseDTO<ModelQueryDTO, ModelDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-排序字段")
    private String descs;

    @ApiModelProperty(value = "4-排序字段")
    private String ascs;

    /**
    * 站点id
    */
    @ApiModelProperty(value = "3-站点id" )
    private Long siteId;
    /**
    * 模型名称
    */
    @ApiModelProperty(value = "4-模型名称" )
    private String modelName;
    /**
    * 模型表名称
    */
    @ApiModelProperty(value = "5-模型表名称" )
    private String tableName;
    /**
    * 内容模型，栏目模型
    */
    @ApiModelProperty(value = "6-内容模型，栏目模型" )
    private Integer modelType;
    /**
    * 字段描述
    */
    @ApiModelProperty(value = "7-字段描述" )
    private String des;
    /**
    * 状态
    */
    @ApiModelProperty(value = "8-状态" )
    private Boolean status;

    public Page<ModelDO> page() {
        Page<ModelDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<ModelQueryDTO, ModelDO> converter = new Converter<ModelQueryDTO, ModelDO>() {
        @Override
        public ModelDO doForward(ModelQueryDTO modelQueryDTO) {
            return WarpsUtils.copyTo(modelQueryDTO, ModelDO.class);
        }

        @Override
        public ModelQueryDTO doBackward(ModelDO modelDO) {
            return WarpsUtils.copyTo(modelDO, ModelQueryDTO.class);
        }
    };

    @Override
    public ModelDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ModelQueryDTO convertFor(ModelDO modelDO) {
        return converter.doBackward(modelDO);
    }
}
