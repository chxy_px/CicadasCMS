package com.cicadascms.service.admin.logic.vo;

import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.ModelFieldRule;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * ModelFieldVO对象
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ModelFieldVO对象", description = "模型字段")
public class ModelFieldVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "1-主键")
    private Integer fieldId;
    /**
     * 模型字段
     */
    @ApiModelProperty(value = "2-模型字段")
    private Integer modelId;
    /**
     * 字段名称
     */
    @ApiModelProperty(value = "3-字段名称")
    private String fieldName;
    /**
     * 字段类型
     */
    @ApiModelProperty(value = "4-字段类型")
    private Integer fieldType;
    /**
     * 字段配置
     */
    @ApiModelProperty(value = "6-字段配置")
    private ModelFieldProp<ModelFieldRule, ModelFieldValue> fieldProp;
    /**
     * 是否检索字段
     */
    @ApiModelProperty(value = "7-是否检索字段")
    private Boolean isSearchField;
    /**
     * 字段描述
     */
    @ApiModelProperty(value = "8-字段描述")
    private String des;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "9-更新用户")
    private Integer updateUser;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "10-更新时间")
    private LocalDateTime updateTime;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "11-创建用户")
    private Integer createUser;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "12-创建时间")
    private LocalDateTime createTime;


}
