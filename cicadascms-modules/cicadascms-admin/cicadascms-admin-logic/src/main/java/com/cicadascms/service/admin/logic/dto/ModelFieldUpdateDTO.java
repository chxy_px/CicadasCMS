package com.cicadascms.service.admin.logic.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.ModelFieldRule;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * ModelFieldUpdateDTO对象
 * 模型字段
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="ModelFieldUpdateDTO对象")
public class ModelFieldUpdateDTO extends BaseDTO<ModelFieldUpdateDTO, ModelFieldDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 主键
    */
    @ApiModelProperty(value = "1-主键" )
    private Integer fieldId;
    /**
    * 模型字段
    */
    @ApiModelProperty(value = "2-模型字段" )
    private Integer modelId;
    /**
    * 字段名称
    */
    @ApiModelProperty(value = "3-字段名称" )
    private String fieldName;
    /**
    * 字段类型
    */
    @ApiModelProperty(value = "4-字段类型" )
    private Integer fieldType;
    /**
    * 数据库字段类型
    */
    @ApiModelProperty(value = "5-数据库字段类型" )
    private Integer columnType;
    /**
     * 字段属性
     */
    @ApiModelProperty(value = "5-字段规则")
    private ModelFieldProp<ModelFieldRule, ModelFieldValue> fieldProp;
    /**
    * 是否检索字段
    */
    @ApiModelProperty(value = "7-是否检索字段" )
    private Boolean isSearchField;
    /**
    * 字段描述
    */
    @ApiModelProperty(value = "8-字段描述" )
    private String des;

    public static Converter<ModelFieldUpdateDTO, ModelFieldDO> converter = new Converter<ModelFieldUpdateDTO, ModelFieldDO>() {
        @Override
        public ModelFieldDO doForward(ModelFieldUpdateDTO modelFieldUpdateDTO) {
            ModelFieldDO modelFieldDO = WarpsUtils.copyTo(modelFieldUpdateDTO, ModelFieldDO.class);
            assert modelFieldDO != null;
            modelFieldDO.setFieldConfig(Fn.toJson(modelFieldUpdateDTO.getFieldProp()));
            return modelFieldDO;
        }

        @Override
        public ModelFieldUpdateDTO doBackward(ModelFieldDO modelFieldDO) {
            return WarpsUtils.copyTo(modelFieldDO, ModelFieldUpdateDTO.class);
        }
    };

    @Override
    public ModelFieldDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ModelFieldUpdateDTO convertFor(ModelFieldDO modelFieldDO) {
        return converter.doBackward(modelFieldDO);
    }
}
