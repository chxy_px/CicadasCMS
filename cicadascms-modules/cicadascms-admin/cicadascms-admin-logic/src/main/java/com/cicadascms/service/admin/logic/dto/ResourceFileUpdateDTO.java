package com.cicadascms.service.admin.logic.dto;

import lombok.Data;
import lombok.ToString;

/**
 * ResourceFileUpdateDTO
 *
 * @author Jin
 */
@Data
@ToString
public class ResourceFileUpdateDTO {

    private String fileName;
    private String filePath;
    private String fileContent;

}
