/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.builder.core.engine;

import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.FileType;
import com.baomidou.mybatisplus.generator.engine.AbstractTemplateEngine;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
import com.cicadascms.builder.core.constant.MyConstVal;
import com.cicadascms.builder.core.po.MyTableInfo;
import lombok.SneakyThrows;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 自定义模板引擎配置
 *
 * @author Jin
 */
public class MyVelocityTemplateEngine extends VelocityTemplateEngine {

    @Override
    public AbstractTemplateEngine mkdirs() {
        return super.mkdirs();
    }

    @SneakyThrows
    @Override
    public AbstractTemplateEngine batchOutput() {
        super.batchOutput();
        List<TableInfo> tableInfoList = getConfigBuilder().getTableInfoList();
        for (TableInfo tableInfo : tableInfoList) {
            Map<String, Object> objectMap = getObjectMap(tableInfo);
            Map<String, String> pathInfo = getConfigBuilder().getPathInfo();
            String entityName = tableInfo.getEntityName();

            //强转为MyTableInfo
            MyTableInfo myTableInfo = (MyTableInfo) tableInfo;

            //生成DTO类
            if (null != myTableInfo.getInputDTOName() && null != pathInfo.get(MyConstVal.DTO_PATH)) {
                String inputDTOFile = String.format((pathInfo.get(MyConstVal.DTO_PATH) + File.separator + myTableInfo.getInputDTOName() + suffixJavaOrKt()), entityName);
                if (isCreate(FileType.OTHER, inputDTOFile)) {
                    writer(objectMap, templateFilePath(MyConstVal.TEMPLATE_INPUT_DTO), inputDTOFile);
                }
            }
            if (null != myTableInfo.getUpdateDTOName() && null != pathInfo.get(MyConstVal.DTO_PATH)) {
                String updateDTOFile = String.format((pathInfo.get(MyConstVal.DTO_PATH) + File.separator + myTableInfo.getUpdateDTOName() + suffixJavaOrKt()), entityName);
                if (isCreate(FileType.OTHER, updateDTOFile)) {
                    writer(objectMap, templateFilePath(MyConstVal.TEMPLATE_UPDATE_DTO), updateDTOFile);
                }
            }
            if (null != myTableInfo.getQueryDTOName() && null != pathInfo.get(MyConstVal.DTO_PATH)) {
                String queryDTOFile = String.format((pathInfo.get(MyConstVal.DTO_PATH) + File.separator + myTableInfo.getQueryDTOName() + suffixJavaOrKt()), entityName);
                if (isCreate(FileType.OTHER, queryDTOFile)) {
                    writer(objectMap, templateFilePath(MyConstVal.TEMPLATE_QUERY_DTO), queryDTOFile);
                }
            }

            //生成VO类
            if (null != myTableInfo.getEntityVOName() && null != pathInfo.get(MyConstVal.VO_PATH)) {
                String voFile = String.format((pathInfo.get(MyConstVal.VO_PATH) + File.separator + myTableInfo.getEntityVOName() + suffixJavaOrKt()), entityName);
                if (isCreate(FileType.OTHER, voFile)) {
                    writer(objectMap, templateFilePath(MyConstVal.TEMPLATE_VO), voFile);
                }
            }

            //生成Wrapper包装类
            if (null != myTableInfo.getWrapperName() && null != pathInfo.get(MyConstVal.WRAPPER_PATH)) {
                String wrapperFile = String.format((pathInfo.get(MyConstVal.WRAPPER_PATH) + File.separator + myTableInfo.getWrapperName() + suffixJavaOrKt()), entityName);
                if (isCreate(FileType.OTHER, wrapperFile)) {
                    writer(objectMap, templateFilePath(MyConstVal.TEMPLATE_WRAPPER), wrapperFile);
                }
            }

        }
        return this;
    }
}
