/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.builder.vo;

import cn.hutool.core.date.DateUtil;
import javafx.scene.image.ImageView;
import lombok.Data;

import java.util.Date;

/**
 * TableVo
 *
 * @author Jin
 */
@Data
public class TableVo {

    private String tableName;
    private String tableComment;
    private String engine;
    private Date createTime;

    public TableInfoVo toTableInfoModel() {
        TableInfoVo tableInfoVo = new TableInfoVo();
        tableInfoVo.tableNameProperty().set(this.getTableName());
        tableInfoVo.tableCommentProperty().set(this.getTableComment() + "(" + this.getTableName() + ")");
        tableInfoVo.engineProperty().set(this.getEngine());
        tableInfoVo.createTimeProperty().set(DateUtil.format(this.getCreateTime(), "yy/MM/dd HH:mm"));
        tableInfoVo.setImageView(getOtherImageView(20, 20));
        return tableInfoVo;
    }


    private ImageView getOtherImageView(int w, int h) {
        ImageView imageView = new ImageView("/icons/public/IconFile.png");
        imageView.setFitHeight(w);
        imageView.setFitWidth(h);
        return imageView;
    }
}
